package com.example.document_sample.locale_interfaces;

import com.example.document_sample.models.Document;

import org.pf4j.ExtensionPoint;

public abstract class LocalDocument implements ExtensionPoint {

    public abstract void updateDocument(Document document);
}