package com.example.document_sample.locale_interfaces;

import com.example.document_sample.models.Receipt;

import org.pf4j.ExtensionPoint;

public abstract class LocalReceipt implements ExtensionPoint {

    public abstract void updateReceipt(Receipt receipt);
}