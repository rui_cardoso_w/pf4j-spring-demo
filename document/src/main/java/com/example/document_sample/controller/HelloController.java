package com.example.document_sample.controller;

import com.example.document_sample.LocalImplementationsHandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	@Autowired
	public LocalImplementationsHandler implementationsHandler;

	@GetMapping("/hello")
	public String onGetHello() {
		implementationsHandler.printHandlers();
		return "olaa";
	}
}