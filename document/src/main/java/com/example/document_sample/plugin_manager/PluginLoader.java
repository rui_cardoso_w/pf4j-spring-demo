package com.example.document_sample.plugin_manager;

import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.pf4j.spring.SpringPluginManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PluginLoader {

	private String pluginsDir;

	@Autowired
	public PluginLoader(@Value("${petro.plugins-dir}") String pluginsDir) {
		this.pluginsDir = pluginsDir;
	}

	@Bean
	public SpringPluginManager pluginManager() {
		URI uri = URI.create(pluginsDir);
		Path paths = Paths.get(uri);
		SpringPluginManager manager = new SpringPluginManager(paths);
		return manager;
	}
}