package com.example.document_sample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.example.document_sample.locale_interfaces.LocalDocument;
import com.example.document_sample.locale_interfaces.LocalReceipt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@DependsOn("pluginManager")
@Component
public class LocalImplementationsHandler {
	private static final Logger logger = LoggerFactory.getLogger(LocalImplementationsHandler.class);

	@Autowired(required = false)
	List<LocalDocument> documentsHandler = new ArrayList<>();
	@Autowired(required = false)
	List<LocalReceipt> receiptsHandler = new ArrayList<>();

	public void printHandlers() {
		logger.info("DOCUMENT: Number of handlers: {}. Handlers name: [{}]", documentsHandler.size(),
				StringUtils.collectionToCommaDelimitedString(
						documentsHandler.stream().map(h -> h.getClass().getSimpleName()).collect(Collectors.toList())));

		logger.info("RECEIPT: Number of handlers: {}. Handlers name: [{}]", receiptsHandler.size(),
				StringUtils.collectionToCommaDelimitedString(
						receiptsHandler.stream().map(h -> h.getClass().getSimpleName()).collect(Collectors.toList())));
	}

}