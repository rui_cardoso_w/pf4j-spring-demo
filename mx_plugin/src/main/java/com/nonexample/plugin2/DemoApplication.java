package com.nonexample.plugin2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

import com.example.document_sample.locale_interfaces.LocalDocument;
import com.example.document_sample.models.Document;

import org.pf4j.Extension;
import org.pf4j.PluginWrapper;
import org.pf4j.spring.SpringPlugin;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class DemoApplication extends SpringPlugin {
	private static final Logger logger = LoggerFactory.getLogger(DemoApplication.class);

	public DemoApplication(PluginWrapper wrapper) {
		super(wrapper);
	}

	@Override
	protected ApplicationContext createApplicationContext() {
		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
		applicationContext.setClassLoader(getWrapper().getPluginClassLoader());
		// applicationContext.register(SpringConfiguration.class);
		applicationContext.refresh();

		return applicationContext;
	}

	@Override
	public void start() {
		System.out.println("HelloPlugin.start()");
	}

	@Override
	public void stop() {
		System.out.println("HelloPlugin.stop()");
		super.stop(); // to close applicationContext
	}

	@Extension
	public static class ESSLocalDocumentImpl extends LocalDocument {

		@Override
		public void updateDocument(Document document) {
			logger.info("Updating document. Initial: " + document);
			document.setLastModified(new Date());
			logger.info("Result document: " + document);
		}

	}
}
